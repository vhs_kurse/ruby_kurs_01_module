## Kontrollstrukturen in Ruby

#### Lernziele

1. Wissen wie die if, else, und unless Anweisungen funktionieren
2. Wissen wie die case Anweisung funktioniert
3. Wissen was Schleifen sind und wie while und until funktionieren
4. Wissen was Code-Blöcke sind
5. Wissen wie man Arrays, Hashes und andere übliche Datenstrukturen in Ruby benutzen kann


#### Uebungen:
1. Mit Statistik Modul anfangen.
2. Einfache Funktionen: mean, median, max, min, range, variance, and standard deviation programmieren.

#### Infos zur Vertiefung

##### 01 Verzweigung mit if, else und unless Anweisungen



##### 02 Verzweigung mit der case Anweisung


##### 03 Schleifen mit while und until Anweisungen



##### 04 Code-Blöcke



##### 05 Array- und List-Datentypen, Hash Datentyp


##### 06 Andere nutzvolle Bausteine
