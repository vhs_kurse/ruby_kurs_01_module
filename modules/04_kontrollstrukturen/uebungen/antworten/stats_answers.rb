require "stats/version"

module Stats

  def self.mean(x)
    index = 0
    sum_total = 0

    # laufe alle elemente des Arrays durch
    while index < x.length

      # addiere das element beim Index `index` zu der Summe
      sum_total += x[index]

      # inkrementiere das Index
      index += 1
    end

    # dividiere die gesamt Summe durch die Anzahl der Elemente im Array
    return sum_total.to_f / x.length
  end

  def self.median(x)
    len = x.length
    x_sorted = x.sort

    if len.even?
      mp = len / 2
      (x_sorted[mp-1] + x_sorted[mp]) / 2.0
    else
      x_sorted[len/2]
    end
  end

  def self.max(x)
    index = 0
    max_value = -10000000

    while index < x.length
      value = x[index]
      if value > max_value
        max_value = value
      end
      index += 1
    end

    max_value
  end

  def self.min(x)
    index = 0
    min_value = x[0]

    while index < x.length
      value = x[index]
      if value < min_value
        min_value = value
      end
      index += 1
    end

    min_value
  end

  def self.range(x)
    self.max(x) - self.min(x)
  end

  def self.variance(x)
    calculated_mean = mean(x)
    index = 0
    sum_of_squares = 0

    while index < x.length
      sum_of_squares += (x[index] - calculated_mean)**2
      index += 1
    end

    return sum_of_squares.to_f / (x.length-1)
  end

  def self.standard_deviation(x)
    Math.sqrt(variance(x))
  end

end
