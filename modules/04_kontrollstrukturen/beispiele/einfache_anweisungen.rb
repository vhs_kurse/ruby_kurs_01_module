# anweisung um text auszugeben
puts "hello world"

# einfache anweisungen

x = 20
y = 30
ergebnis = x + y
puts ergebnis


user_eingeloggt = true
hat_abbonnement = true

if user_eingeloggt
  puts "Willkommen!"
end

if user_eingeloggt && hat_abbonnement
  puts "Lesen Sie die neusten Artikel."
else
  puts "Bitte Kaufen Sie die Abbonnement!"
end

x = 20

if x < 5
  puts "x < 5"
elsif x < 10
  puts "5 <= x < 10"
else
  puts "x >= 10"
end


gewonnen = false

unless gewonnen
  puts "Wollen Sie nochmal spielen?"
end


note = 1

case note
when 1
  puts "Sehr gut"
when 2
  puts "gut"
when 3
  puts "bestanden"
when 4
  puts "knapp"
when 5
  puts "scheisse"
end


require 'socket'

server = TCPServer.new 2000
loop do
  Thread.start(server.accept) do |client_socket|
    client_socket.puts "Hello !"
    client_socket.puts "Time is #{Time.now}"
    client_socket.close
  end
end


# Max wert in Array finden

x = [10,20,5,38, 102, 44, 196, 14]

index = 0
max_wert = -100000

while index < x.length
  value = x[index]

  if value > max_wert
    max_wert = value
  end

  index += 1
end
