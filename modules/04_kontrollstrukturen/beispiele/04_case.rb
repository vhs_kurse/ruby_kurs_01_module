
wert = 1.2

case wert
when 1..10
  puts "zwischen 1 und 10"
else
  puts "wert: #{wert}"
end

passwort = 'hallo'

case passwort
when /^(secret|nichts)$/
  puts "richtig!"
when /^(hallo|welt)$/
  puts "hallo!!"
else
  puts "sorry, das Passwort ist falsch"
end
