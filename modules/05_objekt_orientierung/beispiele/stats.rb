require './csv_reader.rb'

module Stats

class Vector

  def initialize(data)
    @data = data
  end

  def data
    @data
  end

  def self.range(start, ende)
    data = (start..ende).to_a
    ::Stats::Vector.new(data)
  end

  def self.read_csv(path_to_file)
    data = ::Stats::CsvReader.new(path_to_file).read
    ::Stats::Vector.new(data)
  end

  def mean
    data.reduce(&:+).to_f / data.length
  end

end

end
