
# lokale Variable
x = 10
puts x

def simpel
  # das schmeisst einen NameError Fehler
  puts x
end

def simpel_2
  x = 40
  puts x
end


$a = 20

def simple_m
  puts $a # das funktioniert
end

$a = 30
simple_m

$stdout.puts "Hallo Welt!"
$stderr.puts "Fehler!"
