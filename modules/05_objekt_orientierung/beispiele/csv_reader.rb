require 'csv'

module Stats

class CsvReader

  def initialize(file)
    @file = file
  end

  def read
    data = []
    CSV.foreach(@file) do |row|
      value = row[0].to_i
      data.push(value)
    end
    data
  end

end

end
