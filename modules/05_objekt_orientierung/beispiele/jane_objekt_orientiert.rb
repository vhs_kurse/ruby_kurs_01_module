class Shape
end

class Square < Shape
  def initialize(side_length)
    @side_length = side_length
    @@count ||= 0
    @@count += 1
  end

  def area
    @side_length * @side_length
  end

  def perimeter
    @side_length * 4
  end

  def self.count
    @@count
  end
end

a = Square.new(10)
b = Square.new(5)



class Triangle < Shape
  def initialize(base_width, height, s1, s2, s3)
    @base_width, @height = base_width, height
    @side1, @side2, @side3 = s1, s2, s3
  end

  def area
    @base_width * @height / 2
  end

  def perimeter
    @side1 + @side2 + @side3
  end
end

my_square = Square.new(5)
my_triangle = Triangle.new(6, 6, 7.81, 7.81, 7.81)
puts my_square.area
puts my_square.perimeter
puts my_triangle.area
puts my_triangle.perimeter


# Neues Shape Instanz, x referenziert ein Shape-Objekt
