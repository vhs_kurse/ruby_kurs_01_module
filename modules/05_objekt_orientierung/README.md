## Objekt Orientierung

#### Lernziele

1. Wissen was Klassen und Objekte sind
2. Wissen was Lokale, Globale Klass und Instanzvariablen sind
3. Wissen was der Unterschied zwischen Klass und Objektmethoden sind
4. Wissen wie Modules und Namespaces funktionieren
5. Wissen wie Vererbung, Kapselung, Polymorphismus, und Generalisierung funktionieren
6. Reflektion und Mixins kennen


#### Uebungen:
1. Mit Statistik Modul anfangen.
2. Einfache Funktionen: mean, median, max, min, range, variance, and standard deviation programmieren.
3. Alles Objekt orientiert umsetzten.

#### Infos zur Vertiefung

1. [Ruby Users Guide : fange bei OOP Thinking an](http://www.rubyist.net/~slagell/ruby/oothinking.html)
2. [Bastards Book of Ruby : OOP Concepts](http://ruby.bastardsbook.com/chapters/oops/)
3. [Bastards Book of Ruby :Exceptions and Error Handling](http://ruby.bastardsbook.com/chapters/exception-handling/)
