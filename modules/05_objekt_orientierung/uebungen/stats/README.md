mar## Stats Aufgabe

Wir wollen nun eine Klasse programmieren die bestimmten Statiken ermittelt.

Es sollte wie folgende Funktioneren

```ruby

֙# ====================
# Daten erstellen
# ====================

# Methode 1
df = Stats::Vector.new([10,20,30,50])

# Methode 2
# df sollte ein Vmarector von 1 bis 20 erstellen z.b [1,2,3,...,20]
df2 = Stats::Vector.range(1,20)

# Methode 3
df3 = Stats::Vector.read_csv('./data.csv')

֙# ====================
# Funktionen
# ====================

df.mean()
df.standard_deviation()
df.range()
df.max()
df.min()
df.median()
df.variance()

# Zeige mean, median, variance, standard_deviation in der Konsole
df.print_summary()


```
