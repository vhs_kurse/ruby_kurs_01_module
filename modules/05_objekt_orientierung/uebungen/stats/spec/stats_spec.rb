RSpec.describe Stats do

  it "max returns the maximum value from an array" do
    x = [10,25,5,100,17]
    expect(Stats.max(x)).to eq(100)

    x2 = [15,10,5]
    expect(Stats.max(x2)).to eq(15)
  end

  it "min returns the minimum value from an array" do
    x = [10,25,5,100,17]
    expect(Stats.min(x)).to eq(5)

    x2 = [15,10,-22,15]
    expect(Stats.min(x2)).to eq(-22)
  end

  it "range returns the range of an array" do
    x = [-20,-15,-10,5,-2]
    expect(Stats.range(x)).to eq(25)

    x2 = [1000,100,500,-200]
    expect(Stats.range(x2)).to eq(1200)
  end

  it "mean returns the arithmetic mean of an array" do
    x = [5,5,5]
    expect(Stats.mean(x)).to eq(5.0)

    x2 = [10,15,20,25]
    expect(Stats.mean(x2)).to eq(17.5)
  end

  it "median returns the median of an even and odd sized array" do
    x = [1,3,2]
    expect(Stats.median(x)).to eq(2)

    x2 = [15,27,38,2]
    expect(Stats.median(x2)).to eq(21.0)
  end


  it "variance returns the sample variance of an array" do
    x = [5,5,5,5]
    expect(Stats.variance(x)).to eq(0)

    x2 = [10,20,22,16]
    expect(Stats.variance(x2)).to eq(28)
  end

  it "standard_deviation returns the sample standard deviation of an array" do
    x = [5,5,5,5]
    expect(Stats.standard_deviation(x)).to eq(0)

    x2 = [10,20,22,16]
    expect(Stats.standard_deviation(x2)).to be_within(0.005).of(5.29)
  end

end
