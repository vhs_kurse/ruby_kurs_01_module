require 'csv'

class CsvReader

  def initialize(path_to_file)
    @path_to_file = path_to_file
  end

  def read_file
    data = []
    CSV.foreach(@path_to_file) do |row|
      row_value = row.map { |r| r.to_i }.first
      data.push(row_value)
    end
    data
  end

end

class Summary

  def initialize(stats)
    @stats = stats
  end

  def summarize
    x = "#{horizontal_bar}\n"
    x += "# SUMMARY\n"
    x += "#{horizontal_bar}\n"
    x += "Mean: #{@stats.mean}\n"
    x += "Std.Deviation: #{@stats.standard_deviation}"
    x
  end

  def horizontal_bar
    "="*20
  end

end

module Stats

  class Vector

    attr_reader :data

    def initialize(x)
      if !x.is_a?(Array)
        raise ArgumentError, "data must be an Array"
      end
      @data = x
    end

    def self.range(from, to)
      data = (from..to).to_a
      ::Stats::Vector.new(data)
    end

    def self.read_csv(path_to_file)

      # Lese die Daten ein
      data = CsvReader.new(path_to_file).read_file

      # Erstelle ein neues Vector Objekt
      ::Stats::Vector.new(data)
    end

    def mean()
      @data.reduce(:+).to_f / @data.length
    end

    def median()
    end

    def standard_deviation
    end

    def print_summary
      puts Summary.new(self).summarize
    end

  end

end
