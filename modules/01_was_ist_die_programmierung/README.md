## Was ist die Programmierung?

#### Lernziele

1. Wissen wie man algorithmisch denkt.
2. Wissen wie man Probleme in der Informatik angeht.
3. Wissen grob wie Rechner funktionieren
4. Wissen was Algorithmen und Datenstrukturen sind und wie man darüber mehr lernen kann.
5. Ruby und Git installieren.

#### Infos zur Vertiefung

##### 01 Wie ein Rechner funktioniert

1. [Wie ein Rechner funktioniert](https://de.wikibooks.org/wiki/Wikijunior_Computer_und_Internet/_Wie_funktioniert_ein_Computer)
2. [Wie funktioniert ein Computer youtube Serie](https://youtu.be/IhCZu8ALmFE)
3. [Prozessor-Architektur](https://www.elektronik-kompendium.de/sites/com/1310171.htm)
4. [Prozesse im Betriebsystem youtube](https://youtu.be/H5TQ33chWJY)

##### 02 Denken wie ein Rechner

1. [Binärzahlen youtube](https://youtu.be/vRgNi2zqUgA)

##### 03 Algorithmen

1. [Big O Notation youtube](https://youtu.be/__vX2sjlpXU)
2. [Algorithmus einfach erklärt](http://www.giga.de/extra/ratgeber/specials/was-ist-ein-algorithmus-einfach-erklaert/)

##### 04 Datenstrukturen

1. [Array, Queue, Stack](https://www.youtube.com/watch?v=1ulmotGGYJY)
2. [Struct, Baum](https://youtu.be/UDMFnf3Hbqk)

##### 05 Probleme lösen

1. [5 Problem Solving Skills of Great Software Developers](https://www.coderhood.com/5-problem-solving-skills-great-software-developers/)
2. [8 Eigenschaften solltest du als Softwareentwickler haben](https://www.norberteder.com/diese-8-eigenschaften-solltest-du-als-softwareentwickler-haben/)
