---
header-includes:
  - \hypersetup{colorlinks=false,
            allbordercolors={0 0 0},
            pdfborderstyle={/S/U/W 1}}
---

## Installation

#### Virtualbox

1. [Virtualbox installieren](https://www.virtualbox.org/wiki/Downloads)

#### Ruby Linux

Windows:
[Bitnami Ruby Virtual-Machine Installieren](https://bitnami.com/stack/ruby/virtual-machine)
Passwort ist `bitnami123` falls man die Version vom USB installiert hat.

Mac & Linux:

1. [Rbenv](https://github.com/rbenv/rbenv)
2. [Ruby Build](https://github.com/rbenv/ruby-build#readme)

```bash

# zeige Ruby versionen die existieren
rbenv install -l

# installiere eine Version z.B.
rbenv install 2.5.0 -v

```


#### Github / Git

1. [Github Signup](https://github.com/join)
2. [SSH Schlüssel generieren](https://help.github.com/articles/connecting-to-github-with-ssh/)
