class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale


  def set_locale
    if params[:locale].present?
      I18n.locale = params[:locale]
    end
  end

end
