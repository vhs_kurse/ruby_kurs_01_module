
tutorial: http://guides.rubyonrails.org/getting_started.html

1. rails new blog_project
2. Projekt zeigen
3. rails g controller Welcome index

## Artikel Routes

```
routes.rb
- add root 'welcome#index'
- add resources :articles

bundle exec rake routes
```

## Artikel erstellen

rails generate controller Articles

```
def new
end

<%= form_with scope: :article, local: true do |form| %>
  <p>
    <%= form.label :title %><br>
    <%= form.text_field :title %>
  </p>

  <p>
    <%= form.label :text %><br>
    <%= form.text_area :text %>
  </p>

  <p>
    <%= form.submit %>
  </p>
<% end %>

aendern auf url: articles_path

def create
  render plain: params[:article].inspect
end
```

## Model erstellen und Migrations

```
rails generate model Article title:string text:text
rake db:migrate

# model validierungen
validates :title, presence: true

def create
  @article = Article.new(create_params)
  @article.save
  redirect_to @article
end

def show
  @article = Article.find(params[:id])
end

# show content
<p>
  <strong>Titel:</strong>
  <%= @article.title %>
</p>

<p>
  <strong>Inhalt:</strong>
  <%= @article.text %>
</p>

```

## Liste von Artikel

```
def index
  @articles = Article.all
end


<h1>Artikelliste</h1>

<table>
  <tr>
    <th>Titel</th>
    <th>Inhalt</th>
  </tr>

  <% @articles.each do |article| %>
    <tr>
      <td><%= article.title %></td>
      <td><%= article.text %></td>
      <td><%= link_to 'Show', article_path(article) %></td>
    </tr>
  <% end %>
</table>
```

## Links hinzufuegen

```
<h1>Hallo, Rails!</h1>
<%= link_to 'Mein Blog', controller: 'articles' %>

# articles/index
<%= link_to 'Neuer Artikel', new_article_path %>

# articles/new
<%= link_to 'Zurück', articles_path %>
```

## Validierungen

```
validates :title, :text, presence: true, length: { minimum: 3 }

def create
  if @article.save
    redirect_to @article
  else
    render 'new'
  end
end

def new
   @article = Article.new
end

# new.erb
<% if @article.errors.any? %>
   <div id="error_explanation">
     <h2>
       <%= pluralize(@article.errors.count, "error") %> prohibited
       this article from being saved:
     </h2>
     <ul>
       <% @article.errors.full_messages.each do |msg| %>
         <li><%= msg %></li>
       <% end %>
     </ul>
   </div>
 <% end %>
```

## Artikel Aktualisierungen

```

# articles_controller

def edit
  @article = Article.find(params[:id])
end

def update
  @article = Article.find(params[:id])

  if @article.update(article_params)
    redirect_to @article
  else
    render 'edit'
  end
end

# edit.html.erb
<h1>Artikel bearbeiten</h1>

<%= form_with(model: @article,. local: true) do |form| %>

  <% if @article.errors.any? %>
    <div id="error_explanation">
      <h2>
        <%= pluralize(@article.errors.count, "error") %> prohibited
        this article from being saved:
      </h2>
      <ul>
        <% @article.errors.full_messages.each do |msg| %>
          <li><%= msg %></li>
        <% end %>
      </ul>
    </div>
  <% end %>

  <p>
    <%= form.label :title %><br>
    <%= form.text_field :title %>
  </p>

  <p>
    <%= form.label :text %><br>
    <%= form.text_area :text %>
  </p>

  <p>
    <%= form.submit %>
  </p>

<% end %>

<%= link_to 'Zurück', articles_path %>


## index.html.erb

      <td><%= link_to 'Bearbeiten', edit_article_path(article) %></td>

```


## Loesche einen Artikel

```
<td><%= link_to 'Destroy', article_path(article),
              method: :delete,
              data: { confirm: 'Are you sure?' } %></td>

def destroy
  @article = Article.find(params[:id])
  @article.destroy
  redirect_to articles_path
end
```


## Zweites Model

Fuege Comment Modell hinzu
```
rails g model Comment commenter:string body:text article:references
rake db:migrate

resources :articles do
  resources :comments
end

## show.html.erb
<h2>Add a comment:</h2>
<%= form_with(model: [ @article, @article.comments.build ], local: true) do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>

<%= link_to 'Edit', edit_article_path(@article) %> |
<%= link_to 'Back', articles_path %>

rails g controller Comments

class CommentsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end

```


## Comments Erweitern

```
<h2>Comments</h2>
<% @article.comments.each do |comment| %>
  <p>
    <strong>Commenter:</strong>
    <%= comment.commenter %>
  </p>

  <p>
    <strong>Comment:</strong>
    <%= comment.body %>
  </p>
<% end %>

```


## Refactoring

```
app/views/comments/_comment.html.erb

<p>
  <strong>Commenter:</strong>
  <%= comment.commenter %>
</p>

<p>
  <strong>Comment:</strong>
  <%= comment.body %>
</p>

app/views/articles/show.html.erb
<h2>Comments</h2>
<%= render @article.comments %>

app/views/comments/_form.htm.erb
<%= form_with(model: [ @article, @article.comments.build ]) do |form| %>
  <p>
    <%= form.label :commenter %><br>
    <%= form.text_field :commenter %>
  </p>
  <p>
    <%= form.label :body %><br>
    <%= form.text_area :body %>
  </p>
  <p>
    <%= form.submit %>
  </p>
<% end %>

```

## Loesche Kommentare

```
comments/comment
<p>
  <%= link_to 'Destroy Comment', [comment.article, comment],
               method: :delete,
               data: { confirm: 'Are you sure?' } %>
</p>

def destroy
  @article = Article.find(params[:article_id])
  @comment = @article.comments.find(params[:id])
  @comment.destroy
  redirect_to article_path(@article)
end

Article
has_many :comments, dependent: :destroy
```

## Sicherheit

```
ArticlesController

http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
```
