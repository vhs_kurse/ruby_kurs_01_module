## Was sind Programmiersprachen?

#### Lernziele

1. Wissen wie Programmiersprachen funktionieren
2. Wissen was der Unterschied zwischen einen Compiler und einen Interpreter ist
3. Wissen grob wie die Ruby Programmiersprache funktioniert
4. Wissen wie das Ruby Ökosystem aussieht
5. Wissen wie Zahlen und Ausdrücke, Texte und Zeichenketten funktionieren

#### Infos zur Vertiefung

##### 01 Compiler und Interpreter

1. [Geschichte der Programmiersprachen, eine Kurze Zussamenfassung auf Wikipedia](https://de.wikipedia.org/wiki/Geschichte_der_Programmiersprachen)
2. [Vom Quellcode zum Prozessor youtube Video (15 min)](https://youtu.be/cX5XLc9e_g4)
3. [Compiler und Interpreter kurz erklärt](https://www.elektronik-kompendium.de/sites/com/1705231.htm)

##### 02 Der Pfad durch einen Compiler (bzw. Ruby)

1. [Skripte zu Compiler](https://www.cs.hs-rm.de/~weber/fachsem/vortraege/kapitel1.pdf)
2. [How Ruby Interprets your programs](http://blog.honeybadger.io/how-ruby-interprets-and-runs-your-programs/)

##### 03 Das Ruby Ökosystem

1. [Ruby Ecosystem in Y Minutes](https://learnxinyminutes.com/docs/ruby-ecosystem/)
2. [An Introduction to the Ruby Ecosystem](https://infertux.com/posts/2012/10/19/an-introduction-to-the-ruby-ecosystem/)
3. [The History of Ruby](https://www.sitepoint.com/history-ruby/)

##### 04 Testen und Debugging in Ruby

1. [Simple Unit Testing in Ruby](https://en.wikibooks.org/wiki/Ruby_Programming/Unit_testing)
2. [Pry Debugging](http://pryrepl.org/)

##### 05 Zahlen und Ausdrücke

1. [Numbers in Basterds Book of Ruby](http://ruby.bastardsbook.com/chapters/numbers/)
2. [Zahlen in Ruby](https://de.wikibooks.org/wiki/Ruby-Programmierung:_Zahlen)

##### 06 Texte und Zeichenketten

1. [Zeichenketten in Ruby](http://wiki.ruby-portal.de/String)
2. [Working with Strings in Ruby](https://www.digitalocean.com/community/tutorials/how-to-work-with-strings-in-ruby)
