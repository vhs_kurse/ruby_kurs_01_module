# Aufgaben zu Rubygems

#### Aufgabe 1

Pry ist wahrscheinlich das beste Debugging-Werkzeug für Ruby-Anwendungen.
Diese Aufgabe geht darum zu lernen wie man ein Gem wie Pry installiert und anwendet.

1. Gehe auf rubygems.org.
2. Versuche `pry` zu finden.
3. Geh auf das Github Repository um rauszufinden wie man es benutzen kann.
4. Finde auf rubygems.org wie man es installiert.
5. Spiele damit rum.

#### Aufgabe 2

Diese Aufgabe geht darum, dir zu zeigen wie man ein Gem findet, die dir dabei hilft eine Aufgabe zu lösen.


So geht man normalerweise vor.

1. Geh auf google, github, oder rubygems und suche nach ruby statistics.
2. Finde ein Rubygem, das dir gefällt.
3. Installiere das Gem mit gem install [gem-name].
4. Mache ein IRB-Session auf, und lade das Gem mit require '[gem-name]'

Finde ein Gem um folgendes Problem zu lösen.

##### Problem:

Ein Profi Basketballspieler hat in 50 Spielen gespielt und folgende Punktezahl pro Spiel erreicht.

```ruby
x = [21,22,46,24,12,18,18,16,18,19,16,9,17,40,32,18,20,19,16,28,19, 16, 16, 19, 17,
25,22,15,38,51,28,35,22,19,43,27,29,21,17,16,8,29,22,30,11,32,13,19,31,19]
```

Er muss jetzt seinen Vertrag neu verhandeln und fragt dich, seinen Statistiker ihm folgende Werte vorzubereiten
befor er in die Verhandlungen geht.

1. Was ist der Arithmetischer Mittelwert?
2. Was ist die Standardabweichung?
