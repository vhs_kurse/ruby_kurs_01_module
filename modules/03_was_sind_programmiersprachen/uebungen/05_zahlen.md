# Aufgaben zu Zahlen

Hinweisse zu den Aufgaben:

In ruby sind alle Objekte Klassen. Man kann Typ eines Objektes rausfinden, in dem man die methode `.class` auf ein Objekt aufruft.

```ruby

# sehe das x den Klassentyp : Integer hat
x = 45
x.class

x = nil
x.class

x = true
x.class
```

Man kann eine Reihe von Anweisungen mit einer Funktion zusammenfassen. Wir werden später mehr über Funktionen lernen, aber zuerst hier sind ein Paar beispeile für wie man Anweisungen in einer Funktion zusammenfasst.

```ruby

# addiere 3 Zahlen
x = 25
y = 40
z = 100
x + y + z

# addiere 3 Zahlen mit einer Funktion
def addiere(x,y,z)
  x + y + z
end

addiere(25,40,100)

# finde den Abstand zwischen 2 Punkte
p1 = [0,0]
p2 = [3,4]

abstand1 = Math.sqrt( ((p2[0]-p1[0])**2) + ((p2[1]-p1[1])**2) )

def abstand_ermitteln(p1,p2)
  Math.sqrt( ((p2[0]-p1[0])**2) + ((p2[1]-p1[1])**2) )
end

abstand_ermitteln([0,0], [3,4])
```


#### Aufgabe 1:

In IRB, antworte folgende Fragen.

1. Wie viele Stunden gibt es in einem Jahr?
2. Wie viele Minuten hast du gelebt?
3. Wie viele Sekunden wirst du wahrscheinlich noch leben? (Danach berechne das mit Minuten und Sekunden)
  - Lebenserwartung Männer (https://www.welt-in-zahlen.de/laendervergleich.phtml?indicator=39)
  - Lebenserwartung Frauen (https://www.welt-in-zahlen.de/laendervergleich.phtml?indicator=40)

#### Aufgabe 2:

Schreibe Funktionen die die Funktionalitäten von deinen Antworten in Aufgabe 2 zusammenfassen.

Du solltest eine Datei erstellen, und die folgende drei Methoden implementieren.

```ruby
def stunden_im_jahr()
  # ...
end

def minuten_gelebt(jahre_alt)
  # ...
end

def sekunden_noch_im_leben(jahre_alt)
  # ...
end
```

Wie könntest du die Methode `sekunden_noch_im_leben` anpassen, damit es für 3 zusätzliche Länder funktionieren könnte? (z.B. Frankreich, Japan, und Italien?)

#### Aufgabe 3:

Was denkst du passiert wenn man Integer und Float Zahlen kombiniert?

Try computing these in irb:

1. 3.0 / 2
2. 3 / 2.0
3. 3 / 2
4. 4 ** 2.0
5. 4 ** 2
6. 4.1 % 2
7. 4 % 2

Ist das Ergebnis ein Float und ein Integer? Kannst du rausfinden was für ein Regel es gibt?


#### Aufgabe 4:

Schreibe eine Methode die rausfinden ob eine Zahl gerade und ungerade ist.
(Hinweis: Dokumentation lesen: http://ruby-doc.org/core-2.1.5/Integer.html)

Was passiert wenn man deine Methode ein Float als Argument übergibt?
