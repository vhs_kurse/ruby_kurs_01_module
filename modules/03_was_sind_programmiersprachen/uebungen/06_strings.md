# Aufgaben zu Strings

Link zur Dokumentation: http://ruby-doc.org/core-2.1.4/String.html

#### Aufgabe 1

Was denkst du wird mit folgenden Code passieren? (Probiere es aus)

```ruby
"hello".length + "world".length
```

#### Aufgabe 2

Finde eine Methode (in der Dokumentation für String) um die Zeichenkette "Ich lerne ", an die String "Ruby" voranzustellen.

```ruby
# Beispiel
x = "Ruby"
y = "Ich lerne "
x.<die Methode>(y)
```

#### Aufgabe 3

Schreib eine Methode die alle Vokalen aus einer Zeichenkette entfernt.

```ruby
# Beispiel


def keine_vokalen()
  # ...
end

keine_vokalen("Ruby") # => "Rb" sollte das Ergebnis sein
```


#### Aufgabe 4

Finde raus wie du eine Zeichenkette in eine Zahl (ganze und reelle bzw. Integer und Float) umwandeln kannst un umgekehrt.

```ruby
x = "4.56"
x.<die Methode>() # => 4.56

x = "45"
x.<die Methode>() # => 45

x = 100
x.<die Methode>() # => "100"

x = 10.3
x.<die Methode>() # => "10.3"
```
