# Aufgaben: Compiler und Interpreter

#### Aufgabe 01: Ein Ruby Script

Erstelle eine Datei namens `add.rb`.

Mache die Datei in deinem TextEditor auf.

Schreibe folgenden Code und speichere die Datei. Kommentare kannst du weglassen.

```ruby

# erstelle eine Funktion die Zwei Argumente kriegt, x und y
def add(x,y)
  # addiere x und y und gebe das Ergebnis zurueck an den Aufrufer
  x + y
end

```

Jetzt geh auf die Konsole und fuhre die Datei aus mit

```bash
ruby add.rb
```

Es passiert nichts. Kannst du vermuten warum?

Jetzt lade die Datei in eine IRB-Session.

```bash
# fuehre das IRB program aus
irb

# Jetzt in IRB, lade die add.rb Datei
irb(main):001:0> load './add.rb'

# Nun fuehre die add Funktion aus
irb(main):001:0> add(20, 30)
=> 50
irb(main):001:0>
````

#### Zusammenfassung:

Wir haben die Funktion die wir in der add.rb Datei definiert haben in den Ruby Interpreter geladen.

Der Interpreter hat sofort die Datei geparst und die Funktion in ausfuehrbaren Code umgewandelt.

Dann haben wir in der Konsole die Funktion benutzen konnen.

#### Zusätzliche Aufgaben:

1. Fuege ein `puts "Hello World"` in die add.rb datei hinzu. Lade die Datei in IRB erneut. Was passiert? Warum?
2. Kannst du jetzt die `add.rb` so anpassen dass wenn du `ruby add.rb` von der Konsole ausfuehrst, der Skript das Ergebnis von einem add(100,200) auf die Konsole dann direkt ausgibt?

```bash
ruby add.rb
# und als ergebnis kommt:
300
```
