# Aufgaben: Der Pfad durch einen Kompiler

#### Aufgabe 01: Wie interpretiert Ruby meine Programme?

In dieser Aufgabe solltest du lernen wie Ruby deine Programme interpretiert.

Mache ein IRB-Session auf.

```bash
irb
```

Erstelle eine Datei namens `meinname.rb` und schreibe folgendes rein. (Ohne Kommentare)


```ruby
# Konsole

mein_name = 'John Doe'

# hole das erste Argument an das Programm
# Wenn das argument nicht vorhanden ist konvertiere `nil` in `0` mit .to_i
x = ARGV[0].to_i

# wenn x groesser als 10 ist, dann gib mein_name aus
if x > 10
  puts "Mein name ist #{mein_name}"
else
  puts 'Sorry, die Zahl war nicht groß genug'
end
```

Jetzt fuehre den Skript aus

```bash
> ruby meinname.rb 20
Mein name ist John Doe
> ruby meinname.rb 5
Sorry, die Zahl war nicht groß genug
```

Nun lade die Datei-Inhalte in eine IRB-Session und benutze die Ripper Library um zu sehen wie Ruby mit der Datei bei der ausführung umgeht.

Es gibt grob 4 Phasen in Ruby: Lexing, Parsing, zum Bytecode kompilieren, und Bytecode ausführen. Wir fangen bei der ersten Phase an

#### Phase 1: Lexing

```bash
> irb
```

Dann in IRB:

```ruby
# lese die Dateiinhalte in eine Variable
file_contents = File.read('./meinname.rb')
# Erst ist alles nur eine Zeichenkette, wir sehen den Datentyp mit .class Befehl
puts file_contents.class

# Jetzt wollen wir die Zeichenkette in Tokens aufteilen
# Dafuer brauchen wir ripper, also laden wir die Library mit `require`
require 'ripper'

# Jetzt koennen wir die String in Tokens umwandeln
tokens = Ripper.tokenize(file_contents)

# Was fuer eine Datenstruktur war das?
puts tokens.class

# Wie viele Tokens gibt es
puts tokens.length

# Wie sehen die Tokens aus? (nur eintippen, nicht puts)
tokens

# Was ist das erste Token?
puts tokens[0]

# Kannst du auf weitere tokens Zugreifen? Kommst du auf den letzen?

```

OK. Also zuerst wandelt Ruby die Dateiinhalte in Tokens um. Was kommt als nächstes?

Als nächstes muss Ruby die Tokens in Symbole umwandeln die Ruby versteht.

Noch in der Konsole

```ruby
# lade die pp (pretty print) Library um Array Daten besser anschauen zu koennen
require 'pp'

ergebnis = Ripper.lex(file_contents)

# Was ist das Ergebnis fuer einen Datentyp
# Untersuche das Ergebnis, das sind dann die Namen die Ruby intern hat fuer Zeichnen die man im Code eintippt.
pp ergebnis
```

#### Phase 2 : Parsing

Nachdem Ruby die Datei in Stücke aufgebrochen hat, die es versteht, wird es Zeit die "Wörter" jetzt zu strukturien, d.h. eine gewissen Grammatik und die Zusammenhaenge festzustellen.

In dieser Phase wandelt Ruby die Symbole in einen Baum um, in das sogennante AST (Abstract Syntax Tree), das ist die Grammatik die Ruby wiederum versteht, und so wird dein Programm vom Ruby verstanden.


```ruby
# Parse den Quellcode und untersuche es
pp Ripper.sexp(file_contents)
```

Jetzt wirst du merken es wird langsam schwierig fuer uns Menschen das Ergebnis zu verstehen.

Aber lass uns nun ganz zu Ende gehen

#### Phase 3 : In Bytecode umwandeln

```ruby
# Ruby wandelt als naechstes den AST in interne Befehle um die nur Ruby versteht.
# Vieles wird optimimiert
# sehe mal ob du irgendetwas verstehen kannst von der Ausgabe? Einiges kannst du bestimmt vermuten.
# Was macht vermutlich `putstring` z.B.?
puts RubyVM::InstructionSequence.compile(file_contents).disassemble
```

Und das Ergebnis von `RubyVM` ist das was Ruby am Ende intern ausfuehrt jedes Mal wenn eine Datei geladen wird.

Fuehr den meinname.rb nochmal aus und versuche dich vorzustellen wie der Skript vom Anfang ganz durch zu Ende laeuft.

```bash
ruby meinname.rb
```
