## Git Übungen

Das Ziel ist es mit folgenden Übungen git und github zu lernen.

#### Übung 1 : Die Struktur eines Github Projektes

Gehe zu: [Sinatra](https://github.com/sinatra/sinatra)

Sinatra ist ein Projekt was relativ wichtig ist in der Ruby Welt. Es wird häufig eingesetzt wenn man kleine Webdienste erstellen will die schnell und effizient laufen sollen.

Das Ziel dieser Übung ist zu verstehen wie ein Github-Projekt strukturiert ist.

Wichtig sind folgende Punkte, finde und klick auf sie, versuche zu verstehen was sie tun.

1. Code Tab - zeigt den Projektcode
2. Issues Tab - zeigt Meldungen von Menschen die die Software benutzten z.B. Bugs, Features, Ideen, usw.
3. Pull Requests Tab - zeigt Codeänderungen die von anderen Entwickler gemacht wurden und noch in Review stehen.
4. Insights Tab - Wer was wann am Code gemacht hat
5. Watch, Star - Ob man nachrichten und aktualisierung von dem Projekt haben will
6. Fork - Ob man selber eigene Codeänderungen machen will
7. Commits, Branches, Releases, Contributors und MIT Tab.

Frage den Lehrer falls du irgendetwas nicht verstehst oder eine Erklärung haben willst.

Scrolle nach unten und lese den Anfang von dem `README.md` und schau dir einige Einträge im Table of Contents an falls sie dich interessieren.

# Übung 2: fremdes Repository klonen und betrachten

Klone das Sinatra Repository https://github.com/sinatra/sinatra

Mache ein paar Änderungen an den Dateien des Projekts, oder lösche eine Datei, und benutze  git status, git diff und git checkout (z.B. mit Option -p), um die Änderungen wieder rückgängig zu machen und um dich zu vergewissern, dass der ursprüngliche Zustand wiederhergestellt ist.




#### Übung 3 : Ein erstes Projekt anfangen

Das Ziel dieser Übung ist es zu sehen wie man git benutzten kann um seinen Code zu verwalten und mit anderen Teilen zu koennen.

In einem Terminal oder Konsole führe folgende Anweisungen aus:

```bash

# erstelle ein neues Verzeichnis
mkdir uebung1
cd uebung1/

# wir sind in einem Normalverzeichnis was git kann uns das auch sagen
git status

# ok, also wir haben keine Repository, lass uns eine erstellen
# erstelle eine neue git repository
# 1. was wird ausgegeben?
# 2. was sagt danach ein git status Befehl?
git init

# gucken wir das .git Repository an
# hier werden alle Objekte, Dateien, und Daten gespeichert und versioniert
ls -la
ls -la .git/

# mit git log kann man die Historie von allen Dateien sehen
# was sagt jetzt das Log wenn wir noch keine Historie haben?
git log

# nun, brauchen wir irgendetwas was wir tracken und committen koennen
# erstelle zwei Dateien, damit wir was haben
touch A.rb
touch B.rb

# und jetzt was sagt der git status ueber "untracked files"?
git status

# mach A.rb und B.rb in einen TextEditor auf.
# in A.rb schreibe eine Ruby-Methode
# z.B.
# def meine_add_methode(a,b)
#    return a + b
# end
# Schreibe auch eine Methode in B.rb
# speicher das und schliesse deinen TextEditor.
#
# Frage: hat sich nun git status geaendert?
git status

# nun schiebe A.rb auf den Staging Bereich
git add A.rb

# gib folgenden Befehl ein und versuche zu verstehen was auf der Konsole ausgegeben wird.
# d.h. A.rb ist bereit dafuer gespeichert zu werden, ist aber noch nicht in die .git/ repo verschoben worden
git status

# jetzt koennen wir die Datei committen d.h in die lokale .git/ datenbank speichern
# mit git commit wird ein TextEditor aufgemacht, da solltest du deine Commit Message eintippen
# Wenn du fertig bist, speichere und verlasse den Editor.
# achte darauf was auf die Konsole ausgegeben wird
git commit

# jetzt was sagt uns git log?
git log

# OK. Nun haben wir eine Datei hinzugefuegt und in die .git/ Datenbank gespeichert
#
# AUFGABE: Deine Aufgabe ist es jetzt genau das gleiche mit B.rb zu tun.
#

# Nachdem du mit der obigen Aufgabe fertig bist fuehre folgendes aus
git log

# Jetzt erstelle auf github.com ein neues Projekt.
# Folge die Instructions auf github.com um die neue github-Repository als deine lokale Remote-Repo zu definieren
# das wird dann ungefaehr so aussehen
git remote add origin git@github.com:<yourusername>/<yourprojectname>.git
git push -u origin master

# Danach rufe die URL von deinem Projekt auf github.com auf. Deine Dateien A.rb und B.rb sollten da sein.


# Nachdem du festgestellt hast das die Dateien auf github.com sind! fuehre folgendes aus.
# d.h. loesche uebung1/ und alle seine Inhalte
cd ../
sudo rm -R uebung1/

# jetzt kannst du von github.com dein Projekt wieder clonen und alles auf dem aktuellen Stand bringen
git clone git@github.com:<yourusername>/<yourprojectname>.git
cd <yourprojectname>/

# stelle sicher dass A.rb und B.rb da sind

```


#### Übung 4 : Sich beim anderen Projekt beteiligen

Das Ziel dieser Übung ist es zu sehen wie man git benutzten kann, um Code anpassungen an ein anderes Projekt das einem selber nicht gehört vorzunehmen.

##### Schritt 1:

Gehe auf Github und finde ein Projekt von einem von deinen Mitstudenten. Dann clone es bei dir lokal.


```bash
# clone
git clone git@github.com:<mitstudentnamen>/<projektname>.git
cd <projektname>/

# inspiziere das log
git log
```

##### Schritt 2:

Mache Code Anpassungen bei dir, mach eine Datei auf und aendere die Inhalte. Danach:


```bash
# was sagt git status
git status

# adde die Datei
git add <datei>

# jetzt was sagt git status
git status

# commit das lokal
# -m ist eine Kurzel fuer Message
# somit wird keinen TextEditor aufgemacht und man kann direkt eine Kurze Commit-Message eintippen, tipp ein was du denkst bescreibt das am besten was du gerade gemacht hast.
git commit -m 'meine Commit-Message'

# stelle sicher dass dein Commit zum Log hinzugefuegt wurde
# Wer ist der Autor?
git log

# versuche sich git push auszufuehren
# was passiert warum?
git push -u origin master
```

##### Schritt 3:

Nun haben wir gesehen dass wir nicht direkt Code kopieren und ändern können ohne dass der Besitzer uns drauf Schreiberechte vergibt.

Frage deinen Mitstudenten ob er dass für dich machen will. Er muss auf das github.com-Projekt dann Settings Tab > Collaborators klicken und deine Usernamen eingeben.

Manchmal geht das das ein User man Schreibrechte gibt häufig will er aber nicht. Vielleicht kennt er dich nicht oder du kannst ihn nicht erreichen weil er in einer anderen Zeitzone und ganz anderswo auf der Welt lebt.

Angenommen er hat dir diesmal Zugriff gegeben. Probiere die funktionen `git add`, `git reset`, `git diff`, und `git diff --cached` aus, als du Dateien hinzufügst oder Änderungen vornimmst.

Du kannst auch lesen wie ein Befehl funktioniert mit `git help`.

Wie kann man aber in dem Fall vorgehen, wenn man z.B. einen Fehler im Code gefunden hat, es reparieren will und will dass die Änderungen wieder in den Originalcode reinkommen?

Dafür gibt es das sogenannte "Forking". Was wir in der nächsten Übung lernen werden.


#### Übung 5 : Zusammenarbeiten
Bildet Zweier- oder Dreiergruppen.

Wählt das in Aufgabe 3 entstandene Repository eines Gruppenmitglieds aus. Die anderen Gruppenmitglieder legen sich einen eigenen Klon davon an.

Alle in der Gruppe machen nun unabhängig voneinander kleine Änderungen in den Skripten (z.B. Zahlenwerte, neue Dateien oder sonst was).

Synchronisiert die Repositories mit `git fetch` untereinander und versucht mit `git merge` eine Version zu erzeugen, die alle Änderungen enthält.

  - __einfache Variante__: sprecht euch ab, so dass die einzelnen Änderungen unabhängig voneinander sind, so dass es keine Merge-Konflikte gibt
  - __schwierige Variante__: macht jeweils Änderungen an der gleichen Stelle im Code, so dass voraussichtlich Merge-Konflikte entstehen. Versucht, die Konflikte aufzulösen.
