## Installation

Diese ist eine Anleitung für die Installation von den benötigten Software die man braucht um die Aufgaben durchzuarbeiten und Programme laufen zu lassen.


#### Software die man braucht (egal welche Betriebsystem man hat)

1. Ruby version 2.5.0
2. Git
3. PostgreSQL
4. MySQL
5. Rails 5.1.4

Egal welches Betriebsystem man hat, muss am Ende des Installationsprozesses die oben stehenden Programme auf dem Rechner lauffähig sein, das ist das Endziel.

### WINDOWS ANLEITUNG

Wenn man sich fragt warum es so schwierig ist die Ruby-Programmiersprache auf Windows zu installieren kann man bei Stackoverflow eine ganz gute Zusammenfassung lesen. Das ist vielleicht interessant für die die eines Tages professionell mit Ruby arbeiten wollen.

1. [Limitations of running Ruby/Rails on Windows](https://stackoverflow.com/questions/164896/limitations-in-running-ruby-rails-on-windows)

Da gibt es auch eine Reihe weitere Links die die Ruby und Windows Umgebung erklären.

#### (Option 1) Dualboot Lösung (Hoher Schwierigkeitsgrad)

Meine persönliche Empfehlung ist es eine Version von Ubuntu Linux neben Windows auf dem Rechner zu installieren. Das ist zwar nicht so einfach hinzukriegen aber langfristig ist es die beste Lösung wenn man Software entwickeln will. Linux ist ein Open Source betriebsystem das von Linus Torvalds entwickelt würde und steht als freies zugängliches alternatives Betriebsystem für Benutzer auf der ganzen Welt die wissen wollen was ihre Betriebsysteme mit ihren Daten tun und selbst den Code für ihre eigene Zwecke anpassen wollen. Das kann man nicht unter Windows und nicht ganz unter Mac machen (Apple benutzt zum Teil Open Source Software was man als Programmierer natürlich beliebig anpassen kann).

Diese sogenannte Dualboot-Installation geht nicht ganz so leicht aber für die technikbegeisterte und die Leute die Lust drauf haben zu lernen wie man z.B. eine Festplatte partioniert und zwei Betriebsysteme nebeneinander installiert sind die folgenden Anleitungen ganz gut.

1. [The ultimate Windows 7 and Linux Dualboot guide](https://www.lifewire.com/ultimate-windows-7-ubuntu-linux-dual-boot-guide-2200653)
2. [The ultimate Windows 8 and 10 Linux Dualboot guide](https://www.lifewire.com/ultimate-windows-8-1-ubuntu-dual-boot-guide-2200654)

Dabei kann man entweder die Version Ubuntu 16.04.3 LTS oder die Ubuntu 17.10 installieren. 16.04 ist ältere aber mit LTS (Long Term Support) auch stabilere. 17.10 hat die ganz neuen Features aber kann einige kleine Probleme haben. Ich benutze die 17.10 und habe niemals Probleme gehabt.

#### (Option 2) Funktionert nur unter Windows 10 (Empfohlen für alle die Windows 10 haben)

Für Windows 10 gibt es eine Anleitung wo man alles installieren kann von Ruby und Git bis Rails. Es ist super einfach und eine ganz neue Funktion bei Windows 10. Ich habe diese Lösung sogar auf meiner Version von Windows 10, damit ich Linux programme unter Windows laufen lassen kann (ein Nebeneffekt des Installationsvorgangs)

1. [Windows 10 Ruby 2.5.0 mit Git, MySQL, PostgreSQL, und Rails](https://gorails.com/setup/windows/10).

#### (Option 3) Alle andere Windows-Versionen (7, 8, 8.1)

Jetzt gibt es die Anleitungen (probiere zuerst mit der Youtube Anleitung, die ist super einfach und installiert alles was man braucht). Der zweite Link ist da falls der Youtube-clip aus irgendeinem Grund nicht funktionieren sollte.

1. [Installing Ruby on Rails for Windows youtube](https://www.youtube.com/watch?v=OHgXELONyTQ)
2. [Ruby on Rails Windows mit Git, Postgres, und alle andere Programme](https://medium.com/ruby-on-rails-web-application-development/how-to-install-rubyonrails-on-windows-7-8-10-complete-tutorial-2017-fc95720ee059)

### MAC ANLEITUNG

Jeder der auf einem Mac arbeitet sollte Homebrew installieren und lernen wie es und das Terminalprogramm funktionieren.

1. [Homebrew Startseite durchlesen und auf Weitere Dokumentation klicken und dort FAQs und andere Themen lesen](https://brew.sh/index_de.html)
2. [Homebrew Tutorial](https://www.howtogeek.com/211541/homebrew-for-os-x-easily-installs-desktop-apps-and-terminal-utilities/)
3. [Introduction to Mac OSX Command Line](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line)

Nachdem man Homebrew installiert hat, installiere git, und stell sicher das man sich einen Account auf Github und Gitlab gemacht hat.


Mach die Terminal-Applikation auf und tippe folgendes ein:
```bash

brew install git

## teste das ganze dann mit
## es kann sein man muss die Terminalapp schliessen und neu aufmachen.
git --version
```

##### Software Installationsanleitungen

1. [Ruby,Git, usw, OSX 10.12 Sierra mit Ruby,Rails](https://gorails.com/setup/osx/10.12-sierra)
2. [OSX 10.11 el capitan](https://gorails.com/setup/osx/10.11-el-capitan)
3. [OSX 10.10 yosemite](https://gorails.com/setup/osx/10.10-yosemite)

Es gibt auch ein Paar andere Anleitungen die Teile des Prozesses ganz gut erklären. (Das Youtube-Video ist ganz gut)

1. [How To Install Ruby and Set Up a Local Programming Environment on macOS](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-and-set-up-a-local-programming-environment-on-macos)
2. [Install Ruby on Rails on Mac OSX](https://www.youtube.com/watch?v=3Lp5XP8pWkU)


### LINUX ANLEITUNG

1. Installiere [Rbenv](https://github.com/rbenv/rbenv)
2. Installiere das rbenv plugin [Ruby Build](https://github.com/rbenv/ruby-build#readme)

dann installiere die folgende programme (aptitude)
Für andere Linux versionen siehe [ruby-build wiki](https://github.com/rbenv/ruby-build/wiki)
```bash

# installiere wichtige libraries
apt-get install gcc-6 autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev

# installiere git
apt-get install git

# installiere die richtige ruby version
rbenv install -v 2.5.0

# fertig!
```
